package com.play.time

import com.squareup.leakcanary.LeakCanary
import timber.log.Timber

class DebugApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        LeakCanary.install(this)
    }
}