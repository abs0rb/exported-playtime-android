package com.play.time.domain.discord.repository

import com.play.time.model.discord.DiscordGuild
import com.play.time.model.discord.DiscordUser
import io.reactivex.Maybe

interface DiscordUserRepository {

    fun getCurrentUser(): Maybe<DiscordUser>

    fun getCurrentUserGuilds(): Maybe<List<DiscordGuild>>
}