package com.play.time.domain.steam.repository

import com.play.time.model.steam.SteamFriendList.SteamFriends
import com.play.time.model.steam.SteamPlayerSummaries.SteamPlayers
import io.reactivex.Maybe

interface SteamUserRepository {
    fun getFriendList(steamID: String): Maybe<SteamFriends>
    fun getPlayerSummaries(steamIDs: String): Maybe<SteamPlayers>
}