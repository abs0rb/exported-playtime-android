package com.play.time.domain.discord.interactor

import com.play.time.domain.discord.repository.DiscordUserRepository
import com.play.time.domain.discord.service.DiscordUserService
import com.play.time.model.discord.DiscordGuild
import com.play.time.model.discord.DiscordUser
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DiscordFetchUserData(
        private val service: DiscordUserService
) : DiscordUserRepository {

    override fun getCurrentUser(): Maybe<DiscordUser> {
        return service.getCurrentUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getCurrentUserGuilds(): Maybe<List<DiscordGuild>> {
        return service.getCurrentUserGuilds()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}