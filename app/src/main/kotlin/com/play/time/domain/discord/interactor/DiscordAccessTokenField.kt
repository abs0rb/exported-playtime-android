package com.play.time.domain.discord.interactor

enum class DiscordAccessTokenField(val key: String) {
    CLIENT_ID("client_id"),
    CLIENT_SECRET("client_secret"),
    GRANT_TYPE("grant_type"),
    CODE("code"),
    REDIRECT_URI("redirect_uri")
}