package com.play.time.domain.discord.interactor

import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_AUTH_CLIENT_ID
import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_AUTH_CLIENT_SECRET
import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_AUTH_GRANT_TYPE
import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_AUTH_REDIRECT_URL
import com.play.time.domain.discord.repository.DiscordAuthRepository
import com.play.time.domain.discord.service.DiscordAuthService
import com.play.time.domain.discord.interactor.DiscordAccessTokenField.*
import com.play.time.model.discord.DiscordAccessToken
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DiscordFetchAccessToken(
        private val service: DiscordAuthService
) : DiscordAuthRepository {

    override fun getAccessToken(code: String): Maybe<DiscordAccessToken> {
        val request = getAccessTokenRequest(code)

        return service.getAccessToken(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun getAccessTokenRequest(code: String): Map<String, String> {
        return mapOf(
                Pair(CLIENT_ID.key, DISCORD_AUTH_CLIENT_ID),
                Pair(CLIENT_SECRET.key, DISCORD_AUTH_CLIENT_SECRET),
                Pair(GRANT_TYPE.key, DISCORD_AUTH_GRANT_TYPE),
                Pair(CODE.key, code),
                Pair(REDIRECT_URI.key, DISCORD_AUTH_REDIRECT_URL)

        )
    }
}