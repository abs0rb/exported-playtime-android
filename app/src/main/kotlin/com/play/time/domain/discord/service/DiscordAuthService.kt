package com.play.time.domain.discord.service

import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_AUTH_TOKEN_URL
import com.play.time.model.discord.DiscordAccessToken
import io.reactivex.Maybe
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface DiscordAuthService {

    @FormUrlEncoded
    @POST(DISCORD_AUTH_TOKEN_URL)
    fun getAccessToken(@FieldMap fields: Map<String, String>): Maybe<DiscordAccessToken>
}