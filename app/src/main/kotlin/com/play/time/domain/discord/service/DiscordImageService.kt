package com.play.time.domain.discord.service

import io.reactivex.Maybe
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DiscordImageService {

    @GET("avatars/{user_id}/{user_avatar}.png")
    fun getUserAvatar(
            @Path("user_id") userId: Long,
            @Path("user_avatar") avatarName: String,
            @Query("size") size: Int
    ): Maybe<ResponseBody>

    @GET("embed/avatars/{user_discriminator}.png")
    fun getUserDefaultAvatar(
            @Path("user_discriminator") discriminator: Int,
            @Query("size") size: Int
    ): Maybe<ResponseBody>

    @GET("icons/{guild_id}/{icon_name}.png")
    fun getGuildIcon(
            @Path("guild_id") guildId: Long,
            @Path("icon_name") iconName: String,
            @Query("size") size: Int
    ): Maybe<ResponseBody>
}