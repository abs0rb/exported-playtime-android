package com.play.time.domain.discord.service

import com.play.time.model.discord.DiscordGuild
import com.play.time.model.discord.DiscordUser
import io.reactivex.Maybe
import retrofit2.http.GET

interface DiscordUserService {

    @GET("users/@me")
    fun getCurrentUser(): Maybe<DiscordUser>

    @GET("users/@me/guilds")
    fun getCurrentUserGuilds(): Maybe<List<DiscordGuild>>
}