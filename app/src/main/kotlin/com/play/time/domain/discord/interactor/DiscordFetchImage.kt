package com.play.time.domain.discord.interactor

import com.play.time.common.config.AppProperties
import com.play.time.domain.discord.repository.DiscordImageRepository
import com.play.time.domain.discord.service.DiscordImageService
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody

class DiscordFetchImage(
        private val service: DiscordImageService
) : DiscordImageRepository {

    override fun getUserAvatar(userId: Long, avatarName: String): Maybe<ResponseBody> {
        return service.getUserAvatar(userId, avatarName, AppProperties.IMG_SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getUserDefaultAvatar(discriminator: Int): Maybe<ResponseBody> {
        return service.getUserDefaultAvatar(discriminator, AppProperties.IMG_SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getGuildIcon(guildId: Long, iconName: String): Maybe<ResponseBody> {
        return service.getGuildIcon(guildId, iconName, AppProperties.IMG_SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}