package com.play.time.domain.steam.interactor

import com.play.time.domain.steam.repository.SteamUserRepository
import com.play.time.domain.steam.service.SteamUserService
import com.play.time.model.steam.SteamFriendList.SteamFriends
import com.play.time.model.steam.SteamPlayerSummaries.SteamPlayers
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SteamFetchUserData(
        private val service: SteamUserService
) : SteamUserRepository {

    override fun getFriendList(steamID: String): Maybe<SteamFriends> {
        return service.getFriendList(steamID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.friendslist }
    }


    override fun getPlayerSummaries(steamIDs: String): Maybe<SteamPlayers> {
        return service.getPlayerSummaries(steamIDs)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it.response }
    }
}