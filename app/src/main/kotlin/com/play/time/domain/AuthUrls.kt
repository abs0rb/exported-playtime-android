package com.play.time.domain

import com.play.time.common.config.discord.DiscordProperties
import com.play.time.common.config.discord.DiscordProperties.Companion.AUTH_STATE
import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_AUTH_CLIENT_ID
import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_AUTH_REDIRECT_URL
import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_AUTH_RESPONSE_TYPE
import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_AUTH_SCOPE
import com.play.time.common.config.steam.SteamProperties.Companion.STEAM_AUTH_IDENTIFIER_SELECT
import com.play.time.common.config.steam.SteamProperties.Companion.STEAM_AUTH_MODE
import com.play.time.common.config.steam.SteamProperties.Companion.STEAM_AUTH_REALM_PARAM
import com.play.time.common.config.steam.SteamProperties.Companion.STEAM_AUTH_REDIRECT_URL
import com.play.time.common.config.steam.SteamProperties.Companion.STEAM_OPENID_LOGIN_URL
import com.play.time.common.config.steam.SteamProperties.Companion.STEAM_SPECS_BASE_URL

class AuthUrls {

    companion object {

        val STEAM_AUTH_URL = "$STEAM_OPENID_LOGIN_URL?" +
                "openid.claimed_id=$STEAM_AUTH_IDENTIFIER_SELECT&" +
                "openid.identity=$STEAM_AUTH_IDENTIFIER_SELECT&" +
                "openid.mode=$STEAM_AUTH_MODE&" +
                "openid.ns=$STEAM_SPECS_BASE_URL&" +
                "openid.realm=https://$STEAM_AUTH_REALM_PARAM&" +
                "openid.return_to=$STEAM_AUTH_REDIRECT_URL"

        val DISCORD_AUTH_URL = "${DiscordProperties.DISCORD_AUTH_URL}?" +
                "response_type=$DISCORD_AUTH_RESPONSE_TYPE&" +
                "client_id=$DISCORD_AUTH_CLIENT_ID&" +
                "scope=$DISCORD_AUTH_SCOPE&" +
                "state=$AUTH_STATE&" +
                "redirect_uri=$DISCORD_AUTH_REDIRECT_URL"
    }
}