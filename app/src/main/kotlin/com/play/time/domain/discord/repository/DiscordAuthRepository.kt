package com.play.time.domain.discord.repository

import com.play.time.model.discord.DiscordAccessToken
import io.reactivex.Maybe

interface DiscordAuthRepository {
    fun getAccessToken(code: String): Maybe<DiscordAccessToken>
}