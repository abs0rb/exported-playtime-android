package com.play.time.domain.discord.repository

import io.reactivex.Maybe
import okhttp3.ResponseBody

interface DiscordImageRepository {

    fun getUserAvatar(userId: Long, avatarName: String): Maybe<ResponseBody>

    fun getUserDefaultAvatar(discriminator: Int): Maybe<ResponseBody>

    fun getGuildIcon(guildId: Long, iconName: String): Maybe<ResponseBody>
}