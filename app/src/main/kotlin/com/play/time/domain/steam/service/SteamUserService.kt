package com.play.time.domain.steam.service

import com.play.time.common.config.steam.SteamProperties.Companion.STEAM_FRIEND_LIST_URL
import com.play.time.common.config.steam.SteamProperties.Companion.STEAM_USER_SUMMARY_URL
import com.play.time.model.steam.SteamFriendList
import com.play.time.model.steam.SteamPlayerSummaries
import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Query

interface SteamUserService {

    @GET(STEAM_FRIEND_LIST_URL)
    fun getFriendList(
            @Query("steamid") steamID: String
    ): Maybe<SteamFriendList>

    @GET(STEAM_USER_SUMMARY_URL)
    fun getPlayerSummaries(
            @Query("steamids") steamIDs: String
    ): Maybe<SteamPlayerSummaries>
}