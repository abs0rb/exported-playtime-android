package com.play.time.di.activity

import android.app.Activity
import android.content.Context
import com.play.time.di.annotation.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    @PerActivity
    fun provideActivity(): Context = activity
}