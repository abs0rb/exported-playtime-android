package com.play.time.di.data

import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.play.time.BaseApplication
import com.play.time.common.config.AppProperties.Companion.CACHE_SIZE
import com.play.time.common.config.discord.DiscordHeadersInterceptor
import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_BASE_IMAGE_URL
import com.play.time.common.config.discord.DiscordProperties.Companion.DISCORD_BASE_URL
import com.play.time.common.config.steam.SteamProperties.Companion.STEAM_BASE_URL
import com.play.time.di.annotation.PerApplication
import com.play.time.domain.discord.service.DiscordAuthService
import com.play.time.domain.discord.service.DiscordImageService
import com.play.time.domain.discord.service.DiscordUserService
import com.play.time.domain.steam.service.SteamUserService
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.HttpUrl
import okhttp3.HttpUrl.parse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Named

@Module
class ApiModule {

    companion object {
        const val DISCORD_API = "DiscordApi"
        const val DISCORD_IMAGE_API = "DiscordImageApi"

        const val STEAM_API = "SteamApi"
    }

    @Provides
    @PerApplication
    @Named(DISCORD_API)
    fun provideDiscordUrl(): HttpUrl? = parse(DISCORD_BASE_URL)

    @Provides
    @PerApplication
    @Named(STEAM_API)
    fun provideSteamUrl(): HttpUrl? = parse(STEAM_BASE_URL)

    @Provides
    @PerApplication
    @Named(DISCORD_IMAGE_API)
    fun provideDiscordImageUrl(): HttpUrl? = parse(DISCORD_BASE_IMAGE_URL)

    @Provides
    @PerApplication
    fun provideOkHttpCache(app: BaseApplication): Cache = Cache(app.cacheDir, CACHE_SIZE)

    @Provides
    @PerApplication
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message -> Timber.tag("OkHttp").d(message) }
                .setLevel(BODY)
    }

    @Provides
    @PerApplication
    @Named(DISCORD_API)
    fun provideDiscordOkHttpClient(cache: Cache,
                            logInterceptor: HttpLoggingInterceptor,
                            headersInterceptor: DiscordHeadersInterceptor): OkHttpClient {

        return OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(headersInterceptor)
                .addInterceptor(logInterceptor)
                .build()
    }

    @Provides
    @PerApplication
    fun provideOkHttpClient(cache: Cache,
                            logInterceptor: HttpLoggingInterceptor): OkHttpClient {

        return OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(logInterceptor)
                .build()
    }

    @Provides
    @PerApplication
    @Named(DISCORD_API)
    fun provideDiscordRetrofit(
            @Named(DISCORD_API) url: HttpUrl?,
            gson: Gson,
            @Named(DISCORD_API) okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(url)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @PerApplication
    @Named(STEAM_API)
    fun provideSteamRetrofit(
            @Named(STEAM_API) url: HttpUrl?,
            gson: Gson,
            okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(url)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @PerApplication
    @Named(DISCORD_IMAGE_API)
    fun provideImageRetrofit(
            @Named(DISCORD_IMAGE_API) url: HttpUrl?,
            gson: Gson,
            @Named(DISCORD_API) okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(url)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @PerApplication
    fun provideDiscordAuthService(@Named(DISCORD_API) retrofit: Retrofit): DiscordAuthService {
        return retrofit.create(DiscordAuthService::class.java)
    }

    @Provides
    @PerApplication
    fun provideDiscordUserService(@Named(DISCORD_API) retrofit: Retrofit): DiscordUserService {
        return retrofit.create(DiscordUserService::class.java)
    }

    @Provides
    @PerApplication
    fun provideDiscordImageService(@Named(DISCORD_IMAGE_API) retrofit: Retrofit): DiscordImageService {
        return retrofit.create(DiscordImageService::class.java)
    }

    @Provides
    @PerApplication
    fun provideSteamUserService(@Named(STEAM_API) retrofit: Retrofit): SteamUserService {
        return retrofit.create(SteamUserService::class.java)
    }
}