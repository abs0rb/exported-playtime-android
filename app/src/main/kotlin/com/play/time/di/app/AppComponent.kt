package com.play.time.di.app

import android.support.annotation.MainThread
import com.play.time.BaseApplication
import com.play.time.common.utils.SharedPrefs
import com.play.time.di.annotation.PerApplication
import com.play.time.domain.discord.interactor.DiscordFetchAccessToken
import com.play.time.domain.discord.interactor.DiscordFetchImage
import com.play.time.domain.discord.interactor.DiscordFetchUserData
import com.google.gson.Gson
import com.play.time.domain.steam.interactor.SteamFetchUserData
import dagger.Component
import io.reactivex.Scheduler

@PerApplication
@Component(modules = [AppModule::class])
interface AppComponent {

    fun app(): BaseApplication

    @MainThread
    fun mainThreadScheduler(): Scheduler

    fun preferences(): SharedPrefs

    fun gson(): Gson

    fun discordFetchAccessToken(): DiscordFetchAccessToken

    fun discordFetchUserData(): DiscordFetchUserData

    fun discordFetchImage(): DiscordFetchImage

    fun steamFetchFriendList(): SteamFetchUserData
}