package com.play.time.di

import timber.log.Timber
import java.lang.reflect.Method
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class Injector<in T : Any> {

    companion object {

        private val CACHE = ConcurrentHashMap<Class<*>, HashMap<Class<*>, Method>>()

        private fun putMethodsToCache(componentClass: Class<*>): HashMap<Class<*>, Method>? {
            var methods: HashMap<Class<*>, Method>? = CACHE[componentClass]
            if (methods == null) {
                synchronized(CACHE) {
                    methods = CACHE[componentClass]
                    if (methods == null) {
                        methods = HashMap()
                        for (method in componentClass.methods) {
                            val params = method.parameterTypes
                            if (params.size == 1)
                                methods!!.put(params[0], method)
                        }
                        CACHE.put(componentClass, methods!!)
                    }
                }
            }
            return methods
        }
    }

    fun inject(target: Any, component: T) {
        val componentClass = component.javaClass

        if (!CACHE.containsKey(componentClass)) {
            putMethodsToCache(componentClass)
        }

        val methods = CACHE[componentClass]

        var targetClass: Class<*>? = target.javaClass
        var method: Method? = methods?.get(targetClass)

        while (method == null && targetClass != null) {
            targetClass = targetClass.superclass
            method = methods?.get(targetClass)
        }

        if (method == null) {
            Timber.e("No %s injecting method exists in %s component", target.javaClass, componentClass)
            return
        }

        try {
            method.invoke(component, target)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}