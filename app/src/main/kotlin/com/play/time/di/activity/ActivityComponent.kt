package com.play.time.di.activity

import android.content.Context
import com.play.time.di.annotation.PerActivity
import com.play.time.di.app.AppComponent
import com.play.time.presentation.auth.app.AuthPresenter
import com.play.time.presentation.auth.app.login.LoginPresenter
import com.play.time.presentation.auth.app.register.RegisterPresenter
import com.play.time.presentation.auth.discord.DiscordPresenter
import com.play.time.presentation.auth.steam.SteamPresenter
import com.play.time.presentation.games.GamesPresenter
import com.play.time.presentation.launcher.LauncherPresenter
import com.play.time.presentation.main.MainPresenter
import com.play.time.presentation.main.apis.TabApisPresenter
import com.play.time.presentation.main.friends.TabFriendsPresenter
import dagger.Component

@PerActivity
@Component(
        dependencies = [AppComponent::class],
        modules = [ActivityModule::class]
)
interface ActivityComponent {

    @PerActivity
    fun activity(): Context

    fun inject(presenter: LauncherPresenter)

    fun inject(presenter: GamesPresenter)

    fun inject(presenter: AuthPresenter)

    fun inject(presenter: LoginPresenter)

    fun inject(presenter: RegisterPresenter)

    fun inject(presenter: DiscordPresenter)

    fun inject(presenter: SteamPresenter)

    fun inject(presenter: MainPresenter)

    fun inject(presenter: TabApisPresenter)

    fun inject(presenter: TabFriendsPresenter)
}