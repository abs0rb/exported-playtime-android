package com.play.time.di.app

import android.support.annotation.MainThread
import com.f2prateek.rx.preferences.Preference
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.play.time.BaseApplication
import com.play.time.common.utils.SharedPrefs
import com.play.time.common.utils.SharedPrefs.PreferenceName.DISCORD_ACCESS_TOKEN
import com.play.time.di.annotation.DiscordAccessToken
import com.play.time.di.annotation.PerApplication
import com.play.time.di.data.ApiModule
import com.play.time.domain.discord.interactor.DiscordFetchAccessToken
import com.play.time.domain.discord.interactor.DiscordFetchImage
import com.play.time.domain.discord.interactor.DiscordFetchUserData
import com.play.time.domain.discord.service.DiscordAuthService
import com.play.time.domain.discord.service.DiscordImageService
import com.play.time.domain.discord.service.DiscordUserService
import com.play.time.domain.steam.interactor.SteamFetchUserData
import com.play.time.domain.steam.service.SteamUserService
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

@Module(includes = [ApiModule::class])
class AppModule(private val app: BaseApplication) {

    @Provides
    @PerApplication
    fun provideApp(): BaseApplication = app

    @Provides
    @PerApplication
    @MainThread
    fun provideMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @PerApplication
    @DiscordAccessToken
    fun provideAccessToken(prefs: SharedPrefs): Preference<String> {
        return prefs.getStringPreference(DISCORD_ACCESS_TOKEN)
    }

    @Provides
    @PerApplication
    fun providePreferences(): SharedPrefs = SharedPrefs(app)

    @Provides
    @PerApplication
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @PerApplication
    fun provideDiscordFetchAccessToken(service: DiscordAuthService): DiscordFetchAccessToken {
        return DiscordFetchAccessToken(service)
    }

    @Provides
    @PerApplication
    fun provideDiscordFetchUserData(service: DiscordUserService): DiscordFetchUserData {
        return DiscordFetchUserData(service)
    }

    @Provides
    @PerApplication
    fun provideDiscordFetchImage(service: DiscordImageService): DiscordFetchImage {
        return DiscordFetchImage(service)
    }

    @Provides
    @PerApplication
    fun provideSteamFetchFriendList(service: SteamUserService): SteamFetchUserData {
        return SteamFetchUserData(service)
    }
}