package com.play.time.common.config.discord

import java.util.*

class DiscordProperties {

    companion object {
        val AUTH_STATE = "${UUID.randomUUID()}"

        const val DISCORD_BASE_URL = "https://discordapp.com/api/"
        const val DISCORD_BASE_IMAGE_URL = "https://cdn.discordapp.com/"

        const val DISCORD_AUTH_URL = "${DISCORD_BASE_URL}oauth2/authorize"
        const val DISCORD_AUTH_TOKEN_URL = "${DISCORD_BASE_URL}oauth2/token"
        const val DISCORD_AUTH_REDIRECT_URL = "https://localhost/callback"

        const val DISCORD_AUTH_RESPONSE_TYPE = "code"
        const val DISCORD_AUTH_SCOPE = "identify%20guilds"
        const val DISCORD_AUTH_CLIENT_ID = "381188172639633409"
        const val DISCORD_AUTH_GRANT_TYPE = "authorization_code"
        const val DISCORD_AUTH_CLIENT_SECRET = "AmeeIVjtAci55LqTDeuTkttTpP-n5CMK"
    }
}