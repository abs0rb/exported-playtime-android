package com.play.time.common.config

class AppProperties {

    companion object {
        val CACHE_SIZE = 20 * 1024 * 1024L
        val IMG_SIZE = 256
    }
}