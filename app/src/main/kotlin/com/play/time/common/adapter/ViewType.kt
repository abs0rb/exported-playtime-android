package com.play.time.common.adapter

interface ViewType {
    fun getViewType(): Int
}