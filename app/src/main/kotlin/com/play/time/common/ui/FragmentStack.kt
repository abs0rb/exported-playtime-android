package com.play.time.common.ui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.play.time.R
import com.play.time.presentation.base.BaseActivity
import java.util.*

class FragmentStack(activity: BaseActivity<*, *>,
                    private val containerId: Int
) {

    private val manager: FragmentManager = activity.getSupportFragmentManager()
    private val isEmpty: Boolean get() = manager.backStackEntryCount == 0

    private val fragments: List<Fragment>
        get() {
            val fragments = ArrayList<Fragment>(manager.backStackEntryCount + 1)

            (0 until manager.backStackEntryCount + 1).mapNotNullTo(fragments) {
                manager.findFragmentByTag(indexToTag(it))
            }

            return fragments
        }

    interface OnBackPressedHandlingFragment {
        fun onBackPressed(): Boolean
    }

    fun size(): Int = fragments.size

    fun push(fragment: Fragment) {
        val top = peek()

        if (top != null) {
            manager.beginTransaction()
                    .setCustomAnimations(
                            R.anim.enter_from_right,
                            R.anim.exit_to_left,
                            R.anim.enter_from_left,
                            R.anim.exit_to_right
                    )
                    .remove(top)
                    .add(containerId, fragment, indexToTag(manager.backStackEntryCount + 1))
                    .addToBackStack(null)
                    .commit()
        } else {
            manager.beginTransaction()
                    .add(containerId, fragment, indexToTag(0))
                    .commit()
        }

        manager.executePendingTransactions()
    }

    fun back(): Boolean {
        val top = peek()

        if (top is OnBackPressedHandlingFragment) {
            if ((top as OnBackPressedHandlingFragment).onBackPressed()) {
                return true
            }
        }

        return pop()
    }

    fun pop(): Boolean {
        if (isEmpty) return false
        manager.popBackStackImmediate()

        return true
    }

    fun replace(fragment: Fragment) {
        manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        manager.beginTransaction()
                .replace(containerId, fragment, indexToTag(0))
                .commit()
        manager.executePendingTransactions()
    }

    fun peek(): Fragment? = manager.findFragmentById(containerId)

    private fun getBackFragment(fragment: Fragment): Fragment? {
        val fragments = fragments

        return fragments.indices.reversed()
                .firstOrNull { fragments[it] === fragment && it > 0 }
                ?.let { fragments[it - 1] }
    }

    private fun indexToTag(index: Int): String {
        return Integer.toString(index)
    }
}