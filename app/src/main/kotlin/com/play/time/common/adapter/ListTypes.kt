package com.play.time.common.adapter

object ListTypes {
    const val GAMES = 0
    const val FRIENDS = 1
    const val APIS = 2
}