@file:JvmName("ExtensionsUtils")

package com.play.time.common

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide

fun Context.getColorCompat(id: Int) = ContextCompat.getColor(this, id)

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View =
        LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)

fun ImageView.loadImg(resId: Int) {
    Glide.with(context).load(resId).into(this)
}

fun ImageView.loadImg(url: String) {
    Glide.with(context).load(url).into(this)
}