package com.play.time.common.adapter

import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup

interface ViewTypeDelegateAdapter {

    fun onCreateViewHolder(parent: ViewGroup): ViewHolder

    fun onBindViewHolder(holder: ViewHolder, item: ViewType)
}