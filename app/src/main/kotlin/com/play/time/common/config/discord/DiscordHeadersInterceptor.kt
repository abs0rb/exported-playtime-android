package com.play.time.common.config.discord

import com.play.time.di.annotation.DiscordAccessToken
import com.f2prateek.rx.preferences.Preference
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

class DiscordHeadersInterceptor @Inject constructor(
        @DiscordAccessToken private val accessToken: Preference<String>
) : Interceptor {

    companion object {
        val AUTH = "Authorization"
        val BEARER = "Bearer"
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val builder = chain.request().newBuilder()

        if (accessToken.isSet && originalRequest.header(AUTH).isNullOrEmpty()) {
            builder.header(AUTH, "$BEARER ${accessToken.get()}")
        }

        return chain.proceed(builder.build())
    }
}