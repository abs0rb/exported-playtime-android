package com.play.time.common.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.f2prateek.rx.preferences.Preference
import com.f2prateek.rx.preferences.RxSharedPreferences
import com.google.gson.Gson
import java.util.*

class SharedPrefs(context: Context) {

    private val gson: Gson = Gson()
    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val rxPrefs: RxSharedPreferences

    init {
        rxPrefs = RxSharedPreferences.create(prefs)
    }

    fun save(key: PreferenceName, value: String) {
        rxPrefs.getString(key.name).set(value)
    }

    fun save(key: PreferenceName, values: Set<String>) {
        rxPrefs.getStringSet(key.name).set(values)
    }

    fun save(key: PreferenceName, value: Long?) {
        rxPrefs.getString(key.name).set(value.toString())
    }

    fun save(key: PreferenceName, i: Int) {
        rxPrefs.getString(key.name).set(i.toString())
    }

    fun save(key: PreferenceName, value: Boolean) {
        rxPrefs.getString(key.name).set(value.toString())
    }

    fun saveComplex(key: PreferenceName, item: Any) {
        val json = gson.toJson(item)
        rxPrefs.getString(key.name).set(json)
    }

    fun <T> getComplex(key: PreferenceName, type: Class<T>): T {
        val json = prefs.getString(key.name, "")
        return gson.fromJson(json, type)
    }

    operator fun contains(key: PreferenceName): Boolean = prefs.contains(key.name)

    fun getString(key: PreferenceName): String = prefs.getString(key.name, "")

    fun getStringPreference(key: PreferenceName): Preference<String> = rxPrefs.getString(key.name)

    fun getLong(key: PreferenceName): Long? = prefs.getString(key.name, "-1").toLong()

    fun getStringSetPreference(key: PreferenceName): Preference<Set<String>> = rxPrefs.getStringSet(key.name, HashSet())

    fun isElementPresentInSet(key: PreferenceName, element: String): Boolean? =
            getStringSetPreference(key).get()?.contains(element)

    fun remove(key: PreferenceName): Boolean {
        val editor = prefs.edit()
        editor.remove(key.name)
        return editor.commit()
    }

    fun clearAll() {
        for (pref in PreferenceName.values()) {
            remove(pref)
        }
    }

    enum class PreferenceName {
        DISCORD_ACCESS_TOKEN,
        DISCORD_REFRESH_TOKEN,
        DISCORD_CURRENT_USER,
        DISCORD_CURRENT_USER_GUILDS,

        STEAM_CURRENT_USER_STEAM_ID,
        STEAM_CURRENT_USER_FRIENDS_LIST,
        STEAM_CURRENT_USER_FRIENDS_SUMMARIES
    }
}