package com.play.time.common.utils

import java.net.URL
import java.net.URLDecoder
import java.util.*

class WebViewUtils {

    companion object {

        private const val ENCODING = "UTF-8"

        fun getQueryMap(url: String): Map<String, String> {
            val map = LinkedHashMap<String, String>()
            val pairs = URL(url).query.split("&".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()

            for (pair in pairs) {
                val idx = pair.indexOf("=")
                map.put(
                        URLDecoder.decode(pair.substring(0, idx), ENCODING),
                        URLDecoder.decode(pair.substring(idx + 1), ENCODING)
                )
            }

            return map
        }
    }
}