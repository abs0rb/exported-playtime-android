package com.play.time.common.config.steam

class SteamProperties {

    companion object {

        private const val STEAM_OPENID_BASE_URL = "https://steamcommunity.com/openid/"
        private const val STEAM_API_KEY = "CB406D0A32D2B0E2AE7B9EFE5C62457F"

        const val STEAM_BASE_URL = "http://api.steampowered.com"

        const val STEAM_FRIEND_LIST_URL = STEAM_BASE_URL +
                "/ISteamUser/GetFriendList/v0001/?key=" +
                "$STEAM_API_KEY&" +
                "relationship=friend"

        const val STEAM_USER_SUMMARY_URL = STEAM_BASE_URL +
                "/ISteamUser/GetPlayerSummaries/v0002/?key=" +
                "$STEAM_API_KEY"

        const val STEAM_OPENID_LOGIN_URL = "${STEAM_OPENID_BASE_URL}login"
        const val STEAM_SPECS_BASE_URL = "http://specs.openid.net/auth/2.0"

        const val STEAM_AUTH_IDENTIFIER_SELECT = "$STEAM_SPECS_BASE_URL/identifier_select"
        const val STEAM_AUTH_MODE = "checkid_setup"
        const val STEAM_AUTH_REALM_PARAM = "PlayTime"
        const val STEAM_AUTH_REDIRECT_URL = "https://$STEAM_AUTH_REALM_PARAM/callback"
    }
}