package com.play.time

import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric

class ReleaseApplication : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
    }
}