package com.play.time

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.play.time.di.Injector
import com.play.time.di.activity.ActivityComponent
import com.play.time.di.app.AppComponent
import com.play.time.di.app.AppModule
import com.play.time.di.app.DaggerAppComponent
import io.fabric.sdk.android.Fabric

abstract class BaseApplication : Application() {

    private val injector: Injector<ActivityComponent> = Injector()

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    fun inject(o: Any, component: ActivityComponent) {
        injector.inject(o, component)
    }
}
