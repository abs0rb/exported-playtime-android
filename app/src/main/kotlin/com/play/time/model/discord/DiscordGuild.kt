package com.play.time.model.discord

data class DiscordGuild(
        val id: Long,
        val name: String,
        val icon: String,
        val owner: Boolean,
        val permissions: Long
)