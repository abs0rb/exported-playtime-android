package com.play.time.model.steam

data class SteamFriendList(
        val friendslist: SteamFriends
) {

    data class SteamFriends(
            val friends: List<SteamUser>
    )

    data class SteamUser(
            val steamid: String,
            val relationship: String,
            val friend_since: Long
    )
}