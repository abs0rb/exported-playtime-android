package com.play.time.model.steam

import com.play.time.R

enum class SteamPersonaState(
        val textResId: Int
) {
    OFFLINE(R.string.steam_status_offline),
    ONLINE(R.string.steam_status_online),
    BUSY(R.string.steam_status_busy),
    AWAY(R.string.steam_status_away),
    SNOOZE(R.string.steam_status_snooze),
    LOOKING_TO_TRADE(R.string.steam_status_looking_to_trade),
    LOOKING_TO_PLAY(R.string.steam_status_looking_to_play)
}