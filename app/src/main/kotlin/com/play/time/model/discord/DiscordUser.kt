package com.play.time.model.discord

data class DiscordUser(
        val id: Long,
        val username: String,
        val discriminator: String,
        val avatar: String,
        val verified: Boolean
)