package com.play.time.model.steam

data class SteamPlayerSummaries(
        val response: SteamPlayers
) {
    data class SteamPlayers(
            val players: List<SteamPlayerSummary>
    )

    data class SteamPlayerSummary(
            val steamid: String,
            val personaname: String,
            val profileurl: String,
            val avatar: String,
            val avatarmedium: String,
            val avatarfull: String,
            val personastate: Int,
            val communityvisibilitystate: Int,
            val profilestate: Int,
            val lastlogoff: Long,
            val commentpermission: Int,

            /* Private Data */
            val realname: String,
            val primaryclanid: Long,
            val timecreated: Long,
            val gameid: Long,
            val gameserverip: String,
            val gameextrainfo: String,
            val loccountrycode: String,
            val locstatecode: String,
            val loccityid: Long
    )
}