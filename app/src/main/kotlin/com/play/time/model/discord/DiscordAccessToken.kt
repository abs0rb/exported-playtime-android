package com.play.time.model.discord

import com.google.gson.annotations.SerializedName

data class DiscordAccessToken(

        @SerializedName("access_token")
        val accessToken: String,

        @SerializedName("token_type")
        val tokenType: String,

        val expires: Long,

        @SerializedName("refresh_token")
        val refreshToken: String,

        val scope: String
)