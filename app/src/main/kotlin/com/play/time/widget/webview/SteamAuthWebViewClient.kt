package com.play.time.widget.webview

import android.content.Context
import android.os.Build.VERSION_CODES.N
import android.support.annotation.RequiresApi
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.play.time.common.utils.WebViewUtils.Companion.getQueryMap
import com.play.time.presentation.auth.steam.SteamActivity

class SteamAuthWebViewClient(
        private val context: Context
) : WebViewClient() {

    companion object {
        val IDENTITY = "openid.identity"
        val ERROR = "error="
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        (context as SteamActivity).hideDialog()
    }

    @Suppress("OverridingDeprecatedMember")
    override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
        if (isUrlValidAndHasSteamID(url)) {
            onSteamIDFetched(url)
            return true
        }

        if (isUrlWithError(url)) {
            onCodeError()
            return true
        }

        view?.loadUrl(url)
        return false
    }

    @RequiresApi(N)
    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        val url = request?.url.toString()
        if (isUrlValidAndHasSteamID(url)) {
            onSteamIDFetched(url)
            return true
        }

        if (isUrlWithError(url)) {
            onCodeError()
            return true
        }

        view?.loadUrl(url)
        return false
    }

    private fun isUrlValidAndHasSteamID(url: String): Boolean {
        return getQueryMap(url)[IDENTITY] != null
    }

    private fun onCodeError() {
        //TODO: handle error
    }

    private fun isUrlWithError(url: String): Boolean = url.contains(ERROR)

    private fun onSteamIDFetched(url: String) {
        val steamID = getQueryMap(url)[IDENTITY]?.substringAfterLast("/")
        (context as SteamActivity).onSteamIDFetched(steamID)
    }
}