package com.play.time.widget.webview

import android.content.Context
import android.os.Build.VERSION_CODES.N
import android.support.annotation.RequiresApi
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.play.time.common.config.discord.DiscordProperties.Companion.AUTH_STATE
import com.play.time.common.utils.WebViewUtils.Companion.getQueryMap
import com.play.time.presentation.auth.discord.DiscordActivity

class DiscordAuthWebViewClient(
        private val context: Context
) : WebViewClient() {

    companion object {
        val KEY_STATE = "state"
        val KEY_CODE = "code"
        val ERROR = "?error="
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        (context as DiscordActivity).hideDialog()
    }

    @Suppress("OverridingDeprecatedMember")
    override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
        if (isUrlValidAndHasCode(url)) {
            onCodeFetched(url)
            return true
        }

        if (isUrlWithError(url)) {
            onCodeError()
            return true
        }

        view?.loadUrl(url)
        return false
    }

    @RequiresApi(N)
    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        val url = request?.url.toString()
        if (isUrlValidAndHasCode(url)) {
            onCodeFetched(url)
            return true
        }

        if (isUrlWithError(url)) {
            onCodeError()
            return true
        }

        view?.loadUrl(url)
        return false
    }

    private fun isUrlWithError(url: String): Boolean = url.contains(ERROR)

    private fun onCodeFetched(url: String) {
        val code = getQueryMap(url)[KEY_CODE]!!
        (context as DiscordActivity).presenter.getAccessToken(code)
    }

    private fun onCodeError() {
        (context as DiscordActivity).onAccessTokenError()
    }

    private fun isUrlValidAndHasCode(url: String): Boolean {
        val map = getQueryMap(url)
        return map[KEY_STATE] == AUTH_STATE && map[KEY_CODE] != null
    }
}