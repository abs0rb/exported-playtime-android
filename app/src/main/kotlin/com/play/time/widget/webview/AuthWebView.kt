package com.play.time.widget.webview

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient

@SuppressLint("SetJavaScriptEnabled")
class AuthWebView : WebView {

    constructor(context: Context) : super(context) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView()
    }

    fun setClient(client: WebViewClient) {
        webViewClient = client
    }

    fun loadAuthUrl(url: String) {
        loadUrl(url)
    }

    private fun initView() {
        settings.javaScriptEnabled = true
        settings.setAppCacheEnabled(true)
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.setSupportMultipleWindows(true)
        settings.domStorageEnabled = true
        settings.cacheMode = WebSettings.LOAD_NO_CACHE
    }
}