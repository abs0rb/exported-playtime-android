package com.play.time.presentation.auth.app.login

import com.play.time.presentation.base.Base

interface Login {

    interface View : Base.View

    interface Presenter : Base.Presenter
}