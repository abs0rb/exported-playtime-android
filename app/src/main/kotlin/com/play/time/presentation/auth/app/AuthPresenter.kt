package com.play.time.presentation.auth.app

import com.play.time.presentation.base.BasePresenter

class AuthPresenter : BasePresenter<Auth.View>(), Auth.Presenter