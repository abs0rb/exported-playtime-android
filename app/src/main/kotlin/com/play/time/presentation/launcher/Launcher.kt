package com.play.time.presentation.launcher

import com.play.time.presentation.base.Base

interface Launcher {

    interface View : Base.View {
        fun initAuth()
        fun onAuthTokenPresent()
    }

    interface Presenter : Base.Presenter
}