package com.play.time.presentation.auth.discord

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.play.time.R
import com.play.time.domain.AuthUrls
import com.play.time.presentation.base.BaseActivity
import com.play.time.presentation.main.MainActivity
import com.play.time.widget.webview.DiscordAuthWebViewClient
import kotlinx.android.synthetic.main.webview_auth.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

class DiscordActivity : BaseActivity<DiscordPresenter, Discord.View>(), Discord.View {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, DiscordActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    override fun providePresenter(): DiscordPresenter = DiscordPresenter(getComponent())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webview_auth)

        showLoadingDialog()
        web_view_auth.setClient(DiscordAuthWebViewClient(this))
        web_view_auth.loadAuthUrl(AuthUrls.DISCORD_AUTH_URL)
    }

    override fun onAccessTokenFetched() {
        toast(R.string.auth_complete)
        finishAuth()
    }

    override fun onAccessTokenError() {
        showAuthInfoDialog()
    }

    private fun showAuthInfoDialog() {
        alert(R.string.info_oauth_required) {
            positiveButton(R.string.ok) { it.dismiss() }
            negativeButton(R.string.exit_app) { exitApp() }
        }.show()
    }

    private fun exitApp() {
        finish()
        System.exit(0)
    }

    private fun finishAuth() {
        hideDialog()
        MainActivity.start(this)
    }
}
