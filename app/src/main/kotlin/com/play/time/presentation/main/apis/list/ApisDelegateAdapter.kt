package com.play.time.presentation.main.apis.list

import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import com.play.time.R
import com.play.time.common.adapter.ViewType
import com.play.time.common.adapter.ViewTypeDelegateAdapter
import com.play.time.common.inflate
import com.play.time.common.loadImg
import kotlinx.android.synthetic.main.item_api.view.*

class ApisDelegateAdapter(
        val viewActions: OnViewSelectedListener
) : ViewTypeDelegateAdapter {

    interface OnViewSelectedListener {
        fun onItemSelected(apiId: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder = ApisViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, item: ViewType) {
        holder as ApisViewHolder
        holder.bind(item as ApiListItem)
    }

    inner class ApisViewHolder(parent: ViewGroup) : ViewHolder(parent.inflate(R.layout.item_api)) {

        private val image = itemView.image
        private val text = itemView.text

        fun bind(model: ApiListItem) {
            image.loadImg(model.imageResId)
            text.text = model.text

            super.itemView.setOnClickListener {
                viewActions.onItemSelected(model.id)
            }
        }
    }
}