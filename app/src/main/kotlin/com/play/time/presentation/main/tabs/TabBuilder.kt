package com.play.time.presentation.main.tabs

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.play.time.R
import kotlinx.android.synthetic.main.tab_main.view.*

object TabBuilder {

    fun newTab(context: Context, parent: ViewGroup, item: TabItem): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val tabView = inflater.inflate(R.layout.tab_main, parent, false)

        tabView.label.setText(item.labelId)
        tabView.icon.setImageResource(item.iconId)

        return tabView
    }
}