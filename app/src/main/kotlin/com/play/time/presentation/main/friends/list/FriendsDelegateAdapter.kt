package com.play.time.presentation.main.friends.list

import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import com.play.time.R
import com.play.time.common.adapter.ViewType
import com.play.time.common.adapter.ViewTypeDelegateAdapter
import com.play.time.common.inflate
import com.play.time.common.loadImg
import kotlinx.android.synthetic.main.item_friend.view.*

class FriendsDelegateAdapter(
        val viewActions: OnViewSelectedListener
) : ViewTypeDelegateAdapter {

    interface OnViewSelectedListener {
        fun onItemSelected(steamId: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder = FriendsViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, item: ViewType) {
        holder as FriendsViewHolder
        holder.bind(item as FriendsListItem)
    }

    inner class FriendsViewHolder(parent: ViewGroup) : ViewHolder(parent.inflate(R.layout.item_friend)) {

        private val image = itemView.image
        private val name = itemView.name
        private val status = itemView.status

        fun bind(model: FriendsListItem) {
            image.loadImg(model.avatarUrl)
            name.text = model.nickName
            status.text = model.status

            super.itemView.setOnClickListener {
                viewActions.onItemSelected(model.steamID)
            }
        }
    }
}