package com.play.time.presentation.main.apis

import com.play.time.common.utils.SharedPrefs
import com.play.time.di.activity.ActivityComponent
import com.play.time.presentation.base.BasePresenter
import javax.inject.Inject

class TabApisPresenter(component: ActivityComponent) : BasePresenter<TabApis.View>(), TabApis.Presenter {

    @Inject
    lateinit var prefs: SharedPrefs

    init {
        component.inject(this)
    }
}