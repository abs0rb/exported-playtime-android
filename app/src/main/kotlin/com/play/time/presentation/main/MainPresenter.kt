package com.play.time.presentation.main

import com.play.time.di.activity.ActivityComponent
import com.play.time.presentation.base.BasePresenter

class MainPresenter(component: ActivityComponent) : BasePresenter<Main.View>(), Main.Presenter {

    init {
        component.inject(this)
    }
}