package com.play.time.presentation.main.apis

import com.play.time.presentation.base.Base

interface TabApis {

    interface View : Base.View

    interface Presenter : Base.Presenter
}