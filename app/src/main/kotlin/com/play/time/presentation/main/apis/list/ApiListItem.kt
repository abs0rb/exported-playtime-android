package com.play.time.presentation.main.apis.list

import com.play.time.common.adapter.ListTypes
import com.play.time.common.adapter.ViewType

data class ApiListItem(
        val id: Int,
        val imageResId: Int,
        val text: String
) : ViewType {
    override fun getViewType(): Int = ListTypes.APIS
}