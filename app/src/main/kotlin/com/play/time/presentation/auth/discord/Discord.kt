package com.play.time.presentation.auth.discord

import com.play.time.presentation.base.Base

interface Discord {

    interface View : Base.View {
        fun onAccessTokenFetched()
        fun onAccessTokenError()
    }

    interface Presenter : Base.Presenter {
        fun getAccessToken(code: String)
    }
}