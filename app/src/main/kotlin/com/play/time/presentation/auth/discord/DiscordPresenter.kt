package com.play.time.presentation.auth.discord

import com.play.time.common.utils.SharedPrefs
import com.play.time.common.utils.SharedPrefs.PreferenceName.DISCORD_ACCESS_TOKEN
import com.play.time.common.utils.SharedPrefs.PreferenceName.DISCORD_REFRESH_TOKEN
import com.play.time.di.activity.ActivityComponent
import com.play.time.domain.discord.interactor.DiscordFetchAccessToken
import com.play.time.presentation.base.BasePresenter
import timber.log.Timber
import javax.inject.Inject

class DiscordPresenter(component: ActivityComponent) : BasePresenter<Discord.View>(), Discord.Presenter {

    @Inject
    lateinit var prefs: SharedPrefs

    @Inject
    lateinit var fetchAccessToken: DiscordFetchAccessToken

    init {
        component.inject(this)
    }

    override fun getAccessToken(code: String) {
        view?.showLoadingDialog()
        fetchAccessToken.getAccessToken(code).subscribe(
                {
                    prefs.save(DISCORD_ACCESS_TOKEN, it.accessToken)
                    prefs.save(DISCORD_REFRESH_TOKEN, it.refreshToken)

                    view?.onAccessTokenFetched()
                },
                {
                    Timber.e(it)
                    //TODO: handle getAccessToken exception
                }
        )
    }
}