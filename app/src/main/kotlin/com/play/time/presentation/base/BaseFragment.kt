package com.play.time.presentation.base

import android.widget.TextView
import com.play.time.di.activity.ActivityComponent
import net.grandcentrix.thirtyinch.TiFragment

abstract class BaseFragment<P : BasePresenter<V>, V : Base.View> : TiFragment<P, V>(), Base.View {

    override fun setError(textView: TextView, message: String) {
        getParent().setError(textView, message)
    }

    override fun showMessage(stringId: Int) {
        getParent().showMessage(stringId)
    }

    override fun showMessage(stringId: Int, vararg formatArgs: Any) {
        getParent().showMessage(stringId, formatArgs)
    }

    override fun showMessage(message: String) {
        getParent().showMessage(message)
    }

    override fun showLoadingDialog() {
        getParent().showLoadingDialog()
    }

    override fun showSendingDialog() {
        getParent().showSendingDialog()
    }

    override fun hideDialog() {
        getParent().hideDialog()
    }

    override fun getText(textView: TextView): String = getParent().getText(textView)

    fun getComponent(): ActivityComponent = getParent().getComponent()

    private fun getParent() = (activity as BaseActivity<*, *>)
}