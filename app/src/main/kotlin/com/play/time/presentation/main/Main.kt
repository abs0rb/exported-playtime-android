package com.play.time.presentation.main

import com.play.time.presentation.base.Base

interface Main {

    interface View : Base.View {
        fun setAvatar(bytes: ByteArray)
    }

    interface Presenter : Base.Presenter
}