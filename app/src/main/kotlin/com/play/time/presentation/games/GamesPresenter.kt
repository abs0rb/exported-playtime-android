package com.play.time.presentation.games

import com.play.time.di.activity.ActivityComponent
import com.play.time.presentation.base.BasePresenter

class GamesPresenter(component: ActivityComponent) : BasePresenter<Games.View>(), Games.Presenter {

    init {
        component.inject(this)
    }
}