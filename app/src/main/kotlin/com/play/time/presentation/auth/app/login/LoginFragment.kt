package com.play.time.presentation.auth.app.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.play.time.R
import com.play.time.presentation.auth.app.Auth
import com.play.time.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment<LoginPresenter, Login.View>(), Login.View {

    companion object {
        fun newInstance(): LoginFragment = LoginFragment()
    }

    override fun providePresenter(): LoginPresenter = LoginPresenter(getComponent())

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater?.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signup.setOnClickListener { getParent().showRegisterView() }
        login.setOnClickListener { getParent().login() }
    }

    private fun getParent(): Auth.View = activity as Auth.View
}