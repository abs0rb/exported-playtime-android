package com.play.time.presentation.main.apis.list

import com.play.time.R

enum class Api(
        val imageResId: Int,
        val textResId: Int
) {
    STEAM(R.drawable.ic_steam, R.string.api_steam),
    DISCORD(R.drawable.ic_discord, R.string.api_discord)
}