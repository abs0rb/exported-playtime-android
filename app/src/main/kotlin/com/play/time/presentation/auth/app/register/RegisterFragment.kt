package com.play.time.presentation.auth.app.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.play.time.R
import com.play.time.presentation.auth.app.Auth
import com.play.time.presentation.base.BaseFragment

class RegisterFragment : BaseFragment<RegisterPresenter, Register.View>(), Register.View {

    companion object {
        fun newInstance(): RegisterFragment = RegisterFragment()
    }

    override fun providePresenter(): RegisterPresenter = RegisterPresenter(getComponent())

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater?.inflate(R.layout.fragment_register, container, false)
    }

    private fun getParent(): Auth.View = activity as Auth.View
}