package com.play.time.presentation.main.friends.list

import com.play.time.common.adapter.ListTypes
import com.play.time.common.adapter.ViewType

data class FriendsListItem(
        val steamID: String,
        val avatarUrl: String,
        val nickName: String,
        val status: String
) : ViewType {
    override fun getViewType(): Int = ListTypes.FRIENDS
}