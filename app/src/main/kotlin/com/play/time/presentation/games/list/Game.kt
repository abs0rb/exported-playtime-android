package com.play.time.presentation.games.list

import com.play.time.R

enum class Game(
        val imageResId: Int,
        val bgResId: Int,
        val colorResId: Int,
        val indicatorColorResId: Int
) {
    CSGO(
            R.drawable.game_csgo,
            R.drawable.bg_csgo,
            R.color.csgo,
            R.color.csgo_indicator
    ),
    OVERWATCH(
            R.drawable.game_overwatch,
            R.drawable.bg_overwatch,
            R.color.overwatch,
            R.color.overwatch_indicator
    ),
    DOTA2(
            R.drawable.game_dota2,
            R.drawable.bg_dota2,
            R.color.dota2,
            R.color.dota2_indicator
    ),
    LEAGUE(
            R.drawable.game_league,
            R.drawable.bg_league,
            R.color.league,
            R.color.league_indicator
    ),
    FORTNITE(
            R.drawable.game_fortnite,
            R.drawable.bg_fortnite,
            R.color.fortnite,
            R.color.fortnite_indicator
    ),
    PUBG(
            R.drawable.game_pubg,
            R.drawable.bg_pubg,
            R.color.pubg,
            R.color.pubg_indicator
    ),
    BATTLEFIELD1(
            R.drawable.game_bf1,
            R.drawable.bg_bf1,
            R.color.bf1,
            R.color.bf1_indicator
    )
}