package com.play.time.presentation.games

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager.OnPageChangeListener
import com.play.time.R
import com.play.time.common.getColorCompat
import com.play.time.presentation.base.BaseActivity
import com.play.time.presentation.games.list.Game
import com.play.time.presentation.games.list.GamesAdapter
import kotlinx.android.synthetic.main.activity_games.*

class GamesActivity : BaseActivity<GamesPresenter, Games.View>(), Games.View, OnPageChangeListener {

    companion object {
        const val PAGER_PADDING_HORIZONTAL = 100
        const val PAGER_MARGIN = 30

        fun start(context: Context) {
            val intent = Intent(context, GamesActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    override fun providePresenter(): GamesPresenter {
        return GamesPresenter(getComponent())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_games)

        setupPager()
    }

    private fun setupPager() {
        val adapter = GamesAdapter(this)
        pager.adapter = adapter
        pager.offscreenPageLimit = Game.values().size
        pager.clipToPadding = false
        pager.setPadding(PAGER_PADDING_HORIZONTAL, 0, PAGER_PADDING_HORIZONTAL, 0)
        pager.pageMargin = PAGER_MARGIN
        pager.addOnPageChangeListener(this)
    }

    override fun onPageScrollStateChanged(state: Int) {
        /* no-op */
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        /* no-op */
    }

    override fun onPageSelected(position: Int) {
        val game = Game.values()[position]
        indicator.selectedColor = getColorCompat(game.indicatorColorResId)
        bg.setImageResource(game.bgResId)
        settings.setColorFilter(getColorCompat(game.colorResId))
    }
}