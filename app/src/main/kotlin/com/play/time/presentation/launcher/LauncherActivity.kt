package com.play.time.presentation.launcher

import com.play.time.presentation.auth.app.AuthActivity
import com.play.time.presentation.base.BaseActivity
import com.play.time.presentation.main.MainActivity

class LauncherActivity : BaseActivity<LauncherPresenter, Launcher.View>(), Launcher.View {

    override fun providePresenter(): LauncherPresenter = LauncherPresenter(getComponent())

    override fun initAuth() {
        //TODO: check if already registered
        AuthActivity.start(this)
    }

    override fun onAuthTokenPresent() {
        MainActivity.start(this)
    }
}