package com.play.time.presentation.main.apis.list

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import com.play.time.common.adapter.ListTypes
import com.play.time.common.adapter.ViewType
import com.play.time.common.adapter.ViewTypeDelegateAdapter
import com.play.time.presentation.main.apis.list.ApisDelegateAdapter.OnViewSelectedListener

class ApisAdapter(
        listener: OnViewSelectedListener,
        private val items: List<ViewType>
) : Adapter<ViewHolder>() {

    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()

    init {
        delegateAdapters.put(ListTypes.APIS, ApisDelegateAdapter(listener))
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            delegateAdapters.get(viewType).onCreateViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items[position])
    }

    override fun getItemViewType(position: Int) = items[position].getViewType()
}