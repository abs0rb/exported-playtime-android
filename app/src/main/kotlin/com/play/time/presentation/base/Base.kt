package com.play.time.presentation.base

import android.support.annotation.StringRes
import android.widget.TextView
import net.grandcentrix.thirtyinch.TiView

interface Base {

    interface View : TiView {

        fun setError(textView: TextView, message: String)

        fun showMessage(@StringRes stringId: Int)

        fun showMessage(@StringRes stringId: Int, vararg formatArgs: Any)

        fun showMessage(message: String)

        fun showLoadingDialog()

        fun showSendingDialog()

        fun hideDialog()

        fun getText(textView: TextView): String
    }

    interface Presenter
}