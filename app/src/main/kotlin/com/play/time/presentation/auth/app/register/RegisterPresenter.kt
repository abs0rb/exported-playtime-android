package com.play.time.presentation.auth.app.register

import com.play.time.common.utils.SharedPrefs
import com.play.time.di.activity.ActivityComponent
import com.play.time.presentation.base.BasePresenter
import javax.inject.Inject

class RegisterPresenter(component: ActivityComponent) : BasePresenter<Register.View>(), Register.Presenter {

    @Inject
    lateinit var prefs: SharedPrefs

    init {
        component.inject(this)
    }
}