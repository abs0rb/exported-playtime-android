package com.play.time.presentation.main.apis

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.play.time.R
import com.play.time.common.adapter.ViewType
import com.play.time.presentation.auth.discord.DiscordActivity
import com.play.time.presentation.auth.steam.SteamActivity
import com.play.time.presentation.base.BaseFragment
import com.play.time.presentation.main.apis.list.Api
import com.play.time.presentation.main.apis.list.ApiListItem
import com.play.time.presentation.main.apis.list.ApisAdapter
import com.play.time.presentation.main.apis.list.ApisDelegateAdapter.OnViewSelectedListener
import kotlinx.android.synthetic.main.fragment_tab_apis.*

class TabApisFragment : BaseFragment<TabApisPresenter, TabApis.View>(), TabApis.View, OnViewSelectedListener {

    override fun providePresenter(): TabApisPresenter = TabApisPresenter(getComponent())

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater?.inflate(R.layout.fragment_tab_apis, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initList()
    }

    override fun onItemSelected(apiId: Int) {
        when (apiId) {
            Api.STEAM.ordinal -> SteamActivity.start(activity)
            Api.DISCORD.ordinal -> DiscordActivity.start(activity)
        }
    }

    //TODO: block if already in shared preferences
    private fun initList() {
        list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
        }

        list.adapter = ApisAdapter(this, getListItems())
    }

    private fun getListItems(): List<ViewType> {
        return Api.values().map {
            ApiListItem(
                    it.ordinal, it.imageResId, getString(it.textResId)
            ) as ViewType
        }.toList()
    }
}