package com.play.time.presentation.launcher

import com.play.time.common.utils.SharedPrefs
import com.play.time.common.utils.SharedPrefs.PreferenceName.DISCORD_ACCESS_TOKEN
import com.play.time.di.activity.ActivityComponent
import com.play.time.presentation.base.BasePresenter
import javax.inject.Inject

class LauncherPresenter(component: ActivityComponent) : BasePresenter<Launcher.View>(), Launcher.Presenter {

    @Inject
    lateinit var prefs: SharedPrefs

    init {
        component.inject(this)
    }

    override fun onAttachView(view: Launcher.View) {
        super.onAttachView(view)

        if (prefs.contains(DISCORD_ACCESS_TOKEN)) {
            //TODO: handle expired token
            view.onAuthTokenPresent()
        } else {
            view.initAuth()
        }
    }
}