package com.play.time.presentation.auth.app

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.play.time.R
import com.play.time.common.ui.FragmentStack
import com.play.time.presentation.auth.app.login.LoginFragment
import com.play.time.presentation.auth.app.register.RegisterFragment
import com.play.time.presentation.base.BaseActivity
import com.play.time.presentation.games.GamesActivity
import com.play.time.presentation.main.MainActivity

class AuthActivity : BaseActivity<AuthPresenter, Auth.View>(), Auth.View {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, AuthActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    private val stack = FragmentStack(this, R.id.container)

    override fun providePresenter(): AuthPresenter = AuthPresenter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        stack.push(LoginFragment.newInstance())
    }

    override fun login() {
        GamesActivity.start(this)
//        MainActivity.start(this)
    }

    override fun showRegisterView() {
        stack.push(RegisterFragment.newInstance())
    }
}
