package com.play.time.presentation.auth.steam

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.play.time.R
import com.play.time.domain.AuthUrls
import com.play.time.presentation.base.BaseActivity
import com.play.time.presentation.main.MainActivity
import com.play.time.widget.webview.SteamAuthWebViewClient
import kotlinx.android.synthetic.main.webview_auth.*

class SteamActivity : BaseActivity<SteamPresenter, Steam.View>(), Steam.View {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, SteamActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    override fun providePresenter(): SteamPresenter = SteamPresenter(getComponent())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webview_auth)

        showLoadingDialog()
        web_view_auth.setClient(SteamAuthWebViewClient(this))
        web_view_auth.loadAuthUrl(AuthUrls.STEAM_AUTH_URL)
    }

    override fun onSteamIDFetched(steamID: String?) {
        steamID?.let {
            presenter.saveSteamID(steamID)
            MainActivity.start(this)
        }
    }
}
