package com.play.time.presentation.base

import android.app.AlertDialog
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import com.play.time.BaseApplication
import com.play.time.R
import com.play.time.di.activity.ActivityComponent
import com.play.time.di.activity.ActivityModule
import com.play.time.di.activity.DaggerActivityComponent
import com.play.time.di.app.AppComponent
import dmax.dialog.SpotsDialog
import net.grandcentrix.thirtyinch.TiActivity

@Suppress("DEPRECATION")
abstract class BaseActivity<P : BasePresenter<V>, V : Base.View> : TiActivity<P, V>(), Base.View {

    private lateinit var component: ActivityComponent
    private lateinit var dialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component = getComponent()
    }

    override fun setError(textView: TextView, message: String) {
        textView.error = message
    }

    override fun showMessage(stringId: Int) {
        Toast.makeText(this, stringId, Toast.LENGTH_LONG).show()
    }

    override fun showMessage(stringId: Int, vararg formatArgs: Any) {
        Toast.makeText(this, resources.getString(stringId, *formatArgs), Toast.LENGTH_LONG).show()
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showLoadingDialog() {
        dialog = SpotsDialog(this, getString(R.string.loading))
        dialog.show()
    }

    override fun showSendingDialog() {
        dialog = SpotsDialog(this, getString(R.string.sending))
        dialog.show()
    }

    override fun hideDialog() {
        dialog.dismiss()
    }

    override fun getText(textView: TextView): String = "${textView.text}"

    fun getComponent(): ActivityComponent {
        return DaggerActivityComponent.builder()
                .appComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build()
    }

    private fun getActivityModule(): ActivityModule = ActivityModule(this)

    private fun getApplicationComponent(): AppComponent = (application as BaseApplication).appComponent
}