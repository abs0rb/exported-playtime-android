package com.play.time.presentation.main.friends

import com.play.time.model.steam.SteamPlayerSummaries.SteamPlayerSummary
import com.play.time.presentation.base.Base

interface TabFriends {

    interface View : Base.View {
        fun initList(friends: List<SteamPlayerSummary>)
    }

    interface Presenter : Base.Presenter
}