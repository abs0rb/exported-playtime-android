package com.play.time.presentation.base

import net.grandcentrix.thirtyinch.TiPresenter

abstract class BasePresenter<V : Base.View> : TiPresenter<V>(), Base.Presenter