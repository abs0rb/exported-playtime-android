package com.play.time.presentation.auth.steam

import com.play.time.presentation.base.Base

interface Steam {

    interface View : Base.View {
        fun onSteamIDFetched(steamID: String?)
    }

    interface Presenter : Base.Presenter {
        fun saveSteamID(steamID: String)
    }
}