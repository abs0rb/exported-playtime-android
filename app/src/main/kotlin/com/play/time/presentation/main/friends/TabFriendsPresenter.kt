package com.play.time.presentation.main.friends

import com.play.time.common.utils.SharedPrefs
import com.play.time.common.utils.SharedPrefs.PreferenceName.*
import com.play.time.di.activity.ActivityComponent
import com.play.time.domain.steam.interactor.SteamFetchUserData
import com.play.time.model.steam.SteamFriendList.SteamFriends
import com.play.time.model.steam.SteamPlayerSummaries.SteamPlayerSummary
import com.play.time.model.steam.SteamPlayerSummaries.SteamPlayers
import com.play.time.presentation.base.BasePresenter
import timber.log.Timber
import javax.inject.Inject

class TabFriendsPresenter(component: ActivityComponent) : BasePresenter<TabFriends.View>(), TabFriends.Presenter {

    @Inject
    lateinit var prefs: SharedPrefs

    @Inject
    lateinit var steamFetchUserData: SteamFetchUserData

    init {
        component.inject(this)
    }

    override fun onAttachView(view: TabFriends.View) {
        super.onAttachView(view)

        /* TODO: probably shoud always refresh friends - because of activity status or game played currently */
        if (shouldFetchSteamData()) {
            fetchSteamFriends()
        } else {
            view.initList(getPlayersSummaries())
        }
    }

    private fun fetchSteamFriends() {
        steamFetchUserData.getFriendList(getSteamID())
                .subscribe(
                        {
                            prefs.saveComplex(STEAM_CURRENT_USER_FRIENDS_LIST, it)
                            fetchPlayerSummaries()
                        },
                        { Timber.e(it) }
                )
    }

    private fun fetchPlayerSummaries() {
        val steamIDs = getFriendsSteamIDs()
        steamFetchUserData.getPlayerSummaries(steamIDs)
                .subscribe(
                        {
                            prefs.saveComplex(STEAM_CURRENT_USER_FRIENDS_SUMMARIES, it)
                            view?.initList(it.players)
                        },
                        { Timber.e(it) }
                )
    }

    private fun shouldFetchSteamData(): Boolean {
        val noSteamID = getSteamID().isEmpty()
        val noSavedFriends = getFriendsSteamIDs().isEmpty()

        return noSteamID || noSavedFriends
    }

    private fun getSteamID(): String {
        return prefs.getString(STEAM_CURRENT_USER_STEAM_ID)
    }

    private fun getPlayersSummaries(): List<SteamPlayerSummary> {
        return prefs.getComplex(STEAM_CURRENT_USER_FRIENDS_SUMMARIES, SteamPlayers::class.java)
                .players
    }

    private fun getFriendsSteamIDs(): String {
        return prefs.getComplex(STEAM_CURRENT_USER_FRIENDS_LIST, SteamFriends::class.java)
                .friends
                .map { it.steamid }
                .toList()
                .joinToString(",")
    }
}