package com.play.time.presentation.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.design.widget.TabLayout.OnTabSelectedListener
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager.OnPageChangeListener
import com.play.time.R
import com.play.time.presentation.base.BaseActivity
import com.play.time.presentation.main.tabs.TabAdapter
import com.play.time.presentation.main.tabs.TabBuilder
import com.play.time.presentation.main.tabs.TabItem
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : BaseActivity<MainPresenter, Main.View>(), Main.View, OnTabSelectedListener, OnPageChangeListener {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupTabs()
    }

    override fun providePresenter(): MainPresenter = MainPresenter(getComponent())

    override fun setAvatar(bytes: ByteArray) { /* TODO */ }

    override fun onTabReselected(tab: TabLayout.Tab) { /* no-op */ }

    override fun onTabUnselected(tab: TabLayout.Tab) { /* no-op */ }

    override fun onTabSelected(tab: TabLayout.Tab) {
        pager.currentItem = tab.position
    }

    override fun onPageScrollStateChanged(state: Int) { /* no-op */ }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) { /* no-op */ }

    override fun onPageSelected(position: Int) { /* no-op */ }

    private fun setupTabs() {
        TabItem.values()
                .forEach {
                    val tabView = TabBuilder.newTab(this, content, it)
                    val tab = tabs.newTab().setCustomView(tabView).setTag(tabView)
                    tabs.addTab(tab)
                }

        tabs.addOnTabSelectedListener(this)
        setupPagerForTabs(EnumSet.allOf(TabItem::class.java))
    }

    private fun setupPagerForTabs(tabItems: EnumSet<TabItem>) {
        val adapter = getAdapterForTabs(tabItems)
        pager.adapter = adapter
        pager.currentItem = tabs.selectedTabPosition
        pager.clearOnPageChangeListeners()
        pager.addOnPageChangeListener(this)
        pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
    }

    private fun getAdapterForTabs(tabItems: EnumSet<TabItem>): PagerAdapter {
        val fragments = tabItems
                .map(TabItem::fragment)
                .toList()

        return TabAdapter(supportFragmentManager, fragments)
    }
}