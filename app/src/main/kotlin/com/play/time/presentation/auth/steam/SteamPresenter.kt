package com.play.time.presentation.auth.steam

import com.play.time.common.utils.SharedPrefs
import com.play.time.common.utils.SharedPrefs.*
import com.play.time.common.utils.SharedPrefs.PreferenceName.*
import com.play.time.di.activity.ActivityComponent
import com.play.time.presentation.base.BasePresenter
import javax.inject.Inject

class SteamPresenter(component: ActivityComponent) : BasePresenter<Steam.View>(), Steam.Presenter {

    @Inject
    lateinit var prefs: SharedPrefs

    init {
        component.inject(this)
    }

    override fun saveSteamID(steamID: String) {
        prefs.save(STEAM_CURRENT_USER_STEAM_ID, steamID)
    }
}