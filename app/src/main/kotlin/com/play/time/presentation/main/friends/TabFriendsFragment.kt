package com.play.time.presentation.main.friends

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.play.time.R
import com.play.time.common.adapter.ViewType
import com.play.time.model.steam.SteamPersonaState
import com.play.time.model.steam.SteamPlayerSummaries.SteamPlayerSummary
import com.play.time.presentation.base.BaseFragment
import com.play.time.presentation.main.friends.list.FriendsAdapter
import com.play.time.presentation.main.friends.list.FriendsDelegateAdapter.OnViewSelectedListener
import com.play.time.presentation.main.friends.list.FriendsListItem
import kotlinx.android.synthetic.main.fragment_tab_friends.*

class TabFriendsFragment : BaseFragment<TabFriendsPresenter, TabFriends.View>(), TabFriends.View, OnViewSelectedListener {

    override fun providePresenter(): TabFriendsPresenter = TabFriendsPresenter(getComponent())

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater?.inflate(R.layout.fragment_tab_friends, container, false)
    }

    override fun initList(friends: List<SteamPlayerSummary>) {
        list.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
        }

        list.adapter = FriendsAdapter(this, getListItems(friends))
    }

    override fun onItemSelected(steamId: String) {
        /* TODO */
    }

    private fun getListItems(friends: List<SteamPlayerSummary>): List<ViewType> {
        return friends.map { getFriendsListItem(it) as ViewType }.toList()
    }

    private fun getFriendsListItem(model: SteamPlayerSummary): FriendsListItem {
        return FriendsListItem(
                model.steamid,
                model.avatarfull,
                model.personaname,
                getString(SteamPersonaState.values()[model.personastate].textResId)
        )
    }
}