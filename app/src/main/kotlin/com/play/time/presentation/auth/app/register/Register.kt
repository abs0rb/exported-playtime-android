package com.play.time.presentation.auth.app.register

import com.play.time.presentation.base.Base

interface Register {

    interface View : Base.View

    interface Presenter : Base.Presenter
}