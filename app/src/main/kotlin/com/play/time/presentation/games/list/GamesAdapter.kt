package com.play.time.presentation.games.list

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.play.time.R
import kotlinx.android.synthetic.main.item_game.view.*

class GamesAdapter(
        private val context: Context
) : PagerAdapter() {

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return Game.values().size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val item = Game.values()[position]
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_game, container, false) as ViewGroup
        view.image.setImageResource(item.imageResId)

        container.addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as View)
    }
}