package com.play.time.presentation.main.tabs

import com.play.time.R
import com.play.time.presentation.base.BaseFragment
import com.play.time.presentation.main.apis.TabApisFragment
import com.play.time.presentation.main.friends.TabFriendsFragment

enum class TabItem(
        val labelId: Int,
        val iconId: Int,
        fragmentClass: Class<out BaseFragment<*, *>>
) {
    FRIENDS(
            R.string.tab_friends,
            R.drawable.ic_person_white_24dp,
            TabFriendsFragment::class.java
    ),
    APIS(
            R.string.tab_apis,
            R.drawable.ic_action_internet,
            TabApisFragment::class.java
    );

    val fragment: BaseFragment<*, *> = fragmentClass.newInstance()
}