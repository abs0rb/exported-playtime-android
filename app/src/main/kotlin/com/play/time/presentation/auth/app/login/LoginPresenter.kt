package com.play.time.presentation.auth.app.login

import com.play.time.common.utils.SharedPrefs
import com.play.time.di.activity.ActivityComponent
import com.play.time.presentation.base.BasePresenter
import javax.inject.Inject

class LoginPresenter(component: ActivityComponent) : BasePresenter<Login.View>(), Login.Presenter {

    @Inject
    lateinit var prefs: SharedPrefs

    init {
        component.inject(this)
    }
}