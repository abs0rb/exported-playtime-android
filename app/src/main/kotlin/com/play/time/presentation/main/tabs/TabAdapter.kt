package com.play.time.presentation.main.tabs

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.play.time.presentation.base.BaseFragment

class TabAdapter(
        manager: FragmentManager,
        private val tabFragments: List<BaseFragment<*, *>>
) : FragmentPagerAdapter(manager) {

    override fun getItem(position: Int): BaseFragment<*, *> {
        return tabFragments[position]
    }

    override fun getCount(): Int {
        return tabFragments.size
    }
}