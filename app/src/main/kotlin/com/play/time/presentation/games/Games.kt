package com.play.time.presentation.games

import com.play.time.presentation.base.Base

interface Games {

    interface View : Base.View

    interface Presenter : Base.Presenter
}