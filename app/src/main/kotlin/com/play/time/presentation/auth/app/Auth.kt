package com.play.time.presentation.auth.app

import com.play.time.presentation.base.Base

interface Auth {

    interface View : Base.View {
        fun login()
        fun showRegisterView()
    }

    interface Presenter : Base.Presenter
}