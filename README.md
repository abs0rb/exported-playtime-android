# PlayTime Android Project

Dependencies
-----------------
- Android Annotations
- Android Compat
- Dagger 2 
- RxJava 2
- RxAndroid 2
- Retrofit 2
- JUnit 4
- Robolectric 3
- Mockito Kotlin
- Kluent
- Espresso
- Leak Canary


Gradle tasks
-----------------

Here are some useful Gradle/adb commands:

 * `./gradlew clean build` - Build the entire app and execute unit and integration tests plus lint check.
 * `./gradlew installDebug` - Install the debug apk on the current connected device.
 * `./gradlew deployDebug` - Install and executes the debug apk on the current connected device.
 * `./gradlew runUnitTests` - Execute unit and integration tests. 
 * `./gradlew runAcceptanceTests` - Execute acceptance tests.
